PostgreSQL Kustomization
========================

Launches a single-node PostgreSQL server and storing the data locally.

Default database, admin user and password can be changed in configmap.yaml.
